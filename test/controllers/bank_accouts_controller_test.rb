require 'test_helper'

class BankAccoutsControllerTest < ActionController::TestCase
  setup do
    @bank_accout = bank_accouts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bank_accouts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bank_accout" do
    assert_difference('BankAccout.count') do
      post :create, bank_accout: { accout_number: @bank_accout.accout_number, agency_id: @bank_accout.agency_id, limite: @bank_accout.limite, user_id: @bank_accout.user_id }
    end

    assert_redirected_to bank_accout_path(assigns(:bank_accout))
  end

  test "should show bank_accout" do
    get :show, id: @bank_accout
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bank_accout
    assert_response :success
  end

  test "should update bank_accout" do
    patch :update, id: @bank_accout, bank_accout: { accout_number: @bank_accout.accout_number, agency_id: @bank_accout.agency_id, limite: @bank_accout.limite, user_id: @bank_accout.user_id }
    assert_redirected_to bank_accout_path(assigns(:bank_accout))
  end

  test "should destroy bank_accout" do
    assert_difference('BankAccout.count', -1) do
      delete :destroy, id: @bank_accout
    end

    assert_redirected_to bank_accouts_path
  end
end
