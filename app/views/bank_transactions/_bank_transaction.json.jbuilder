json.extract! bank_transaction, :id, :data_transaction, :value, :type, :user_id, :accout_origin, :accout_destiny, :created_at, :updated_at
json.url bank_transaction_url(bank_transaction, format: :json)
