class DashboardController < ApplicationController
    before_action :set_dependencies, only: [ :index]
    
    
    def index      
    end

    private
    def set_dependencies
        @bank_accouts = BankAccout.where(user_id: current_user.id)
        @bank_transactions = BankTransaction.where(user_id: current_user.id)
    end
end
