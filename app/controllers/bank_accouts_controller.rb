class BankAccoutsController < ApplicationController
  before_action :set_bank_accout, only: [:show, :edit, :update, :destroy]

  # GET /bank_accouts
  # GET /bank_accouts.json
  def index
    @bank_accouts = BankAccout.where(user_id: current_user.id)
  end

  # GET /bank_accouts/1
  # GET /bank_accouts/1.json
  def show
  end

  # GET /bank_accouts/new
  def new
    @bank_accout = BankAccout.new
  end

  # GET /bank_accouts/1/edit
  def edit
  end

  # POST /bank_accouts
  # POST /bank_accouts.json
  def create
    @bank_accout = BankAccout.new(bank_accout_params)
    @bank_accout.user = current_user

    respond_to do |format|
      if @bank_accout.save
        format.html { redirect_to @bank_accout, notice: 'Bank accout was successfully created.' }
        format.json { render :show, status: :created, location: @bank_accout }
      else
        format.html { render :new }
        format.json { render json: @bank_accout.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bank_accouts/1
  # PATCH/PUT /bank_accouts/1.json
  def update
    respond_to do |format|
      if @bank_accout.update(bank_accout_params)
        format.html { redirect_to @bank_accout, notice: 'Bank accout was successfully updated.' }
        format.json { render :show, status: :ok, location: @bank_accout }
      else
        format.html { render :edit }
        format.json { render json: @bank_accout.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bank_accouts/1
  # DELETE /bank_accouts/1.json
  def destroy
    @bank_accout.destroy
    respond_to do |format|
      format.html { redirect_to bank_accouts_url, notice: 'Bank accout was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bank_accout
      @bank_accout = BankAccout.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_accout_params
      params.require(:bank_accout).permit(:accout_number, :limite, :user_id, :agency_id)
    end
end
