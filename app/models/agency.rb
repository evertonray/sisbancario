class Agency < ActiveRecord::Base
  has_many :bank_accout

  validates :agency_number,numericality: {greater_than: 2}, presence: true
  validates :address, presence: true, length: { in: 2..200 }
end
