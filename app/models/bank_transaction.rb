class BankTransaction < ActiveRecord::Base

    belongs_to :accout_origin, class_name: "BankAccout"
    belongs_to :accout_destiny, class_name: "BankAccout"

    enum transaction_type: {
        deposito: 0,
        saque: 1,
        transferencia: 2
    }
    
    validate  :check_limite
    validates :value, numericality: {greater_than: 0}, presence: true
    validates :transaction_type, presence: true

    after_create :deposit, :withdrawal, :transfer
    #Methods
    
    private
    def check_limite
        if (saque? || transferencia?) && value > accout_origin.limite
            errors.add(:value, "Saldo insuficiente")
        end
    
    end

    def check_accout
        if accout_destiny.nil? && accout_origin.nil?
            errors.add(:accout_origin_id, "selecione uma opção")
            errors.add(:accout_destiny_id, "selecione uma opção")
        else
            
        end

    end

    def deposit

        if deposito? 
            accout_origin.update(limite: accout_origin.limite + value)
        end
    end
    
    
    def withdrawal
        if saque?
            accout_origin.update(limite: accout_origin.limite - value)
        end
    end

    def transfer
        if transferencia?
            accout_origin.update(limite: accout_origin.limite - value)
            accout_destiny.update(limite: accout_destiny.limite + value)
        end
    end

end
