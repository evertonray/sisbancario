class BankAccout < ActiveRecord::Base
  belongs_to :user
  belongs_to :agency
  has_many :bank_transactions

  validates :accout_number, numericality: true, presence: true
  validates :limite, numericality: {greater_than: 0}, presence: true
  validates :agency_id, presence: true
  
  #methods
  def show_number_user
    "#{accout_number + ' - ' + user.email } "
  end
end
