$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if (optionValue == 0){
                document.getElementById('bank_transaction_accout_origin_id').style.display='none';
                document.getElementById('bank_transaction_accout_destiny_id').style.display='none';
            }
            if(optionValue == "deposito" || optionValue=="saque"){
                document.getElementById('bank_transaction_accout_origin_id').style.display='block';
                document.getElementById('bank_transaction_accout_destiny_id').style.display='none';
            }
        
            if(optionValue=="transferencia"){ 
                document.getElementById('bank_transaction_accout_destiny_id').style.display='block';
                document.getElementById('bank_transaction_accout_origin_id').style.display='block';
            }
        });
    }).change();
});