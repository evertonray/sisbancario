# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
agencies_list = [
    ["122", "Av. A"],
    ["124", "Av. B"],
    ["125", "Av. C"],
    ["126", "Av. D"],
]

agencies_list.each do |number, address|
    Agency.create(agency_number: number, address: address)
end