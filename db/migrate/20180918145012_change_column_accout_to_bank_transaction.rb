class ChangeColumnAccoutToBankTransaction < ActiveRecord::Migration
  def change
    rename_column(:bank_transactions, :accout_origin, :accout_origin_id)
    rename_column(:bank_transactions, :accout_destiny, :accout_destiny_id)
  end
end
