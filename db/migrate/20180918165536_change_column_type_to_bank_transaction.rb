class ChangeColumnTypeToBankTransaction < ActiveRecord::Migration
  def change
    rename_column(:bank_transactions, :type, :transaction_type)
  end
end
