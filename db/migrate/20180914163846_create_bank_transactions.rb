class CreateBankTransactions < ActiveRecord::Migration
  def change
    create_table :bank_transactions do |t|
      t.date :data_transaction
      t.float :value
      t.integer :type
      t.integer :user_id
      t.integer :accout_origin
      t.integer :accout_destiny

      t.timestamps null: false
    end
  end
end
